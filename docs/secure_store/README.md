# Using secureStore

  ## Keys
  Storing (also retrieving) games from local storage is done via supplying a "key" to SecureStore. The keys that were used to save games to local storage are the following:
  * CurrentGameID - retrieves the ID of the last game that was saved to local storage
  * Game(insert a number Here) - retrieves a game object from local storage
      * example: Game5 retrieves game save object of game #5

  ## Methods
  * secureStore.setItem(key, value)
    * note: value needs to be a string
  * secureStore.getItem(key)
    * if no item exists for the supplied key, null will be recieved
