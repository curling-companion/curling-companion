# Saving a game

## Constructing the game save object

  The game save object is composed of two sections:

  1. metadata

      * game ID
      * chosen outcome
      * game name
      * team score
      * opposing score
      * date (not implemented yet)

  2. stats

### 1. Game save: metadata

  Here is how the metadata is generated:
  1. game ID
      * retrieve the ID of the last game that was saved in local storage
        * if an integer was received as a response, then the new game that is set to be saved in local storage will have the ID of the previous game + 1, meaning the last saved game had an ID of 1, then the new game will have an ID of 2.
        * if null was received as a response,
        this means that we have no games saved in local storage
        as such, make the game ID of the game that we want to save to become 1.

  2. chosen outcome - selected by the user
  3. game name - written by the user
  4. team score - written by the user
  5. opposing score - written by the user
  6. date - not implemented yet

### 2. Game save: stats

  Refer to the diagram below:

  <p align="center">
    <img src="./img/saving_a_game_flowchart.png">
  </p>

## Using secureStore

  ### Keys
  Storing (also retrieving) games from local storage is done via supplying a "key" to SecureStore. The keys that were used to save games to local storage are the following:
  * CurrentGameID - retrieves the ID of the last game that was saved to local storage
  * Game(insert a number Here) - retrieves a game object from local storage
      * example: Game5 retrieves game save object of game #5

  ### Methods
  * secureStore.setItem(key, value)
    * note: value needs to be a string
  * secureStore.getItem(key)
    * if no item exists for the supplied key, null will be recieved

## Process of saving the final game save object to local storage

  1. retrieve the ID of the last game saved in local storage
  2. increment the same ID by 1
  3. build the game save object (metadata, stats)
  4. convert the game save object to JSON string
  5. store the game save object to local storage using the Game(#) key
  6. store the incremented game ID to local storage using the CurrentGameID key to keep track of the ID of the last game saved
