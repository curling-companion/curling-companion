export const SectionsEnum = {
  ROTATION: 'chosenRotation',
  SHOT_TYPE: 'chosenShotType',
  SCORE: 'chosenScore',
  WEIGHT: 'chosenWeightMissed',
  LINE: 'chosenLineMissed',
  SWEEP_ERROR: 'sweepError',
  LINE_ERROR: 'lineError',
};

export const rotationOpts = [
  { label: 'In-turn', value: 0 },
  { label: 'Out-turn', value: 1 }
];

export const shotTypeOpts = [
  { label: 'Guard', value: 0 },
  { label: 'Draw', value: 1 },
  { label: 'Hit', value: 2 }
];

export const scoreOpts = [
  { label: '0', value: 0 },
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '3', value: 3 },
  { label: '4', value: 4 },
];

export const missedWeightOpts = [
  { label: 'Light', value: 0 },
  { label: 'Heavy', value: 1 },
];

export const missedLineOpts = [
  { label: 'Inside', value: 0 },
  { label: 'Wide', value: 1 },
];

export const sweepErrorOpts = [
  { label: 'Sweeping Error', value: 0 },
];

export const lineErrorOpts = [
  { label: 'Line-call Error', value: 0 },
];
