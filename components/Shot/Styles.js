import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    backgroundColor: '#e5ffff',
    paddingBottom: hp('1%')
  },
  cardContainer: {
    backgroundColor: '#e5ffff',
    borderRadius: 10,
    width: wp('100%'),
    marginTop: hp('1.5%'),
    alignItems: 'center',
  },
  card: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: .9,
  },
  optionTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#aa1b1c',
    borderRadius: 50,
    height: hp('5%'),
    marginTop: hp('2%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    marginBottom: hp('2.5%'),
    shadowColor: '#adadc3',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1
  },
  optionText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp('4.3%'),
  },
  labelContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  labelText: {
    fontSize: wp('3.5%'),
    lineHeight: hp('3.4%'),
    color: 'black',
    marginRight: wp('2%')
  },
  missedOptionText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('5.3%'),
  },
  missedTitleContainer: {
    alignItems: 'center',
    marginBottom: hp('1%'),
  }
});

export default styles;
