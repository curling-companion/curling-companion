import React, { Component }  from 'react'
import styles from "./Styles";
import { View, Text, KeyboardAvoidingView,} from 'react-native';
import RadioForm, { RadioButtonLabel, RadioButton, RadioButtonInput } from "react-native-simple-radio-button";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { SectionsEnum, rotationOpts, missedLineOpts, missedWeightOpts, scoreOpts, shotTypeOpts, lineErrorOpts, sweepErrorOpts } from './ShotOptions';
import {
  TextField,
} from 'react-native-material-textfield';
import * as Haptics from 'expo-haptics';

Object.freeze(SectionsEnum);

class Shot extends Component {
  constructor() {
    super();
    this.state = {

    }
  }

  // This function returns the value of the state selected so that we can update the radio button on the UI
  returnSelectedState = (optionName) =>
    optionName === SectionsEnum.ROTATION ? this.props.shotStats.chosenRotation :
    optionName === SectionsEnum.SHOT_TYPE ? this.props.shotStats.chosenShotType :
    optionName === SectionsEnum.SCORE ? this.props.shotStats.chosenScore :
    optionName === SectionsEnum.WEIGHT ? this.props.shotStats.chosenWeightMissed :
    optionName === SectionsEnum.LINE ? this.props.shotStats.chosenLineMissed :
    optionName === SectionsEnum.SWEEP_ERROR ? this.props.shotStats.sweepError :
    optionName === SectionsEnum.LINE_ERROR ? this.props.shotStats.lineError :
    console.log('Cannot return state.');

  renderRadioButtons = (option, optionName) => {
    return (
      <RadioForm
        formHorizontal={true}
        animation={false}
      >
        {
          option.map((obj, i) => (
            <RadioButton
              labelHorizontal={false}
              key={i}
            >
              <RadioButtonInput
                obj={obj}
                index={i}
                onPress={() => {
                  if (global.vibrate) Haptics.impactAsync('light')
                  this.props.handleRadioPress(optionName, obj.value)
                }}
                isSelected={this.returnSelectedState(optionName) === i}
                borderWidth={wp('.5%')}
                buttonInnerColor={'#A0060F'}
                buttonOuterColor={'#A0060F'}
                buttonSize={wp('5%')}
                buttonOuterSize={wp('7.4%')}
                buttonStyle={{}}
                buttonWrapStyle={{}}
              />
              <RadioButtonLabel
                obj={obj}
                index={i}
                onPress={() => this.props.handleRadioPress(optionName, obj.value)}
                labelHorizontal={true}
                labelStyle={styles.labelText}
                labelWrapStyle={{}}
              />
            </RadioButton>
          ))
        }
      </RadioForm>
    )
  };

  showMissedShotOpt() {
    return (
      <View style={styles.cardContainer}>
        <View style={styles.card}>
          <View style={styles.optionTitle}>
            <Text style={styles.optionText}>Missed Shot</Text>
          </View>
          <View style={styles.labelContainer}>
            <View style={styles.missedTitleContainer}>
              <Text style={styles.missedOptionText}>Weight</Text>
            </View>
            <View>
              {this.renderRadioButtons(missedWeightOpts, SectionsEnum.WEIGHT)}
            </View>
            <View style={styles.missedTitleContainer}>
              <Text style={styles.missedOptionText}>Line</Text>
            </View>
            <View>
              {this.renderRadioButtons(missedLineOpts, SectionsEnum.LINE)}
            </View>
          </View>
          <View style={styles.labelContainer}>
            <View style={styles.missedTitleContainer}>
              <Text style={styles.missedOptionText}>Other</Text>
            </View>
              <View>
                {this.renderRadioButtons(sweepErrorOpts, SectionsEnum.SWEEP_ERROR)}
                {this.renderRadioButtons(lineErrorOpts, SectionsEnum.LINE_ERROR)}
                <TextField
                  label='Comments'
                  onChangeText={text => this.props.handleTextInput(text)}
                  containerStyle={styles.textFieldContainer}
                  value={this.props.otherError}
                />
              </View>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.cardContainer}>
            <View style={styles.card}>
              <View style={styles.optionTitle}>
                <Text style={styles.optionText}>Rotation</Text>
              </View>
              <View style={styles.labelContainer}>
                {this.renderRadioButtons(rotationOpts, SectionsEnum.ROTATION)}
              </View>
            </View>
          </View>
          <View style={styles.cardContainer}>
            <View style={styles.card}>
              <View style={styles.optionTitle}>
                <Text style={styles.optionText}>Shot Type</Text>
              </View>
              <View style={styles.labelContainer}>
                {this.renderRadioButtons(shotTypeOpts, SectionsEnum.SHOT_TYPE)}
              </View>
            </View>
          </View>
          <View style={styles.cardContainer}>
            <View style={styles.card}>
              <View style={styles.optionTitle}>
                <Text style={styles.optionText}>Score</Text>
              </View>
              <View style={styles.labelContainer}>
                {this.renderRadioButtons(scoreOpts, SectionsEnum.SCORE)}
              </View>
            </View>
          </View>
        {(this.props.shotStats.chosenScore === 0) ?  this.showMissedShotOpt(): null}
      </View>
    );
  }
};

export default Shot;
