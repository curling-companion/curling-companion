import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  scrollStyle: {
    flexGrow: 1,
    width: "100%",
    paddingBottom: 80,
    alignItems: 'center',
    backgroundColor: "#E5FFFF"
  },
  gameContainer: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    paddingBottom: wp('3%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: .9,
    marginTop: hp('1.5%'),
  },
  gameTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#aa1b1c',
    borderRadius: 50,
    height: hp('5%'),
    marginTop: hp('2%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    shadowColor: '#adadc3',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1
  },
  box: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    marginTop: hp('1.5%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 1,
    shadowOpacity: .9
  },
  buttonText2: {
    color: "white",
    fontWeight: 'bold',
  },
  gameTitleText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp('4.3%'),
  },
  dataRow: {
    flexDirection: "row",
    justifyContent: "center"
  },
  gameDataText: {
    fontSize: hp("2%"),
    padding: wp("3%"),
  },
  spinnerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    justifyContent: "center"
  },
  button1: {
    height: hp("3%"),
    width: hp("3%"),
    borderRadius: hp("6%"),
    backgroundColor: "#7E0006",
    justifyContent: 'center',
    alignItems: 'center'

  },
  headerText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('5%'),
  },
  boxHeader: {
    flexDirection: 'row',
    width: wp('90%'),
    justifyContent: 'space-between',
    padding: hp('.6%')
  },
  chartContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  chart: {
    marginVertical: 8,
    borderRadius: 16
  },
  button: {
    height: hp("5%"),
    width: wp("35%"),
    borderRadius: wp("10%"),
    marginTop: wp('3%'),
    backgroundColor: "#7e0006",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  buttonText: {
    color: "white",
    fontSize: hp("2%")
  },
  chartTitleText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('4.3%'),
  }
});

export default styles;
