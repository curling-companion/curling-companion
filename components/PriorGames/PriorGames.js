import React, { Component } from "react";
import styles from "./Styles";
import { View, Text, TouchableOpacity, ScrollView, ActivityIndicator } from "react-native";
import {
  listenOrientationChange,
  removeOrientationListener,
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import createSecureStore from "@neverdull-agency/expo-unlimited-secure-store";
import {LineChart} from 'react-native-chart-kit';
import * as Haptics from 'expo-haptics';


const secureStore = createSecureStore()

class PriorGames extends Component {

  constructor() {
    super();
    this.state = {
      chosenEnds: 6,
      gameArray: [],
      gamesLoaded: false,
      showLifetimeStats: false,
    };
  }

  async componentDidMount() {
    listenOrientationChange(this);
    await this.loadGames()
    this.setState({
      gamesLoaded: true
    });
  }

  componentWillUnMount() {
    removeOrientationListener();
  }

  loadGames = async() => {
    const getLastGamePlayedIDKey = 'CurrentGameID'
    const lastGamePlayedID = parseInt(
      await secureStore.getItem(getLastGamePlayedIDKey)
    );
    for (let game = lastGamePlayedID; game > 0; game--) {
      const gameKey = 'Game' + game.toString()
      const gameObject = await secureStore.getItem(gameKey)
      this.state.gameArray.push(JSON.parse(gameObject))
    }
  }

  renderGames(game, key) {
    const { navigation } = this.props;
    return (
      <View style={styles.gameContainer} key={key}>
          <View style={styles.gameTitle}>
            <Text style={styles.gameTitleText}>
              #{game["metadata"].ID} - {game["metadata"].gameName}
            </Text>
          </View>
          <View style={styles.dataRow}>
            <Text style={styles.gameDataText}>
              <Text style={{ fontWeight: 'bold' }}>Date: {" "}</Text>
              { game["metadata"].date}
            </Text>
            <Text style={styles.gameDataText}>
              <Text style={{ fontWeight: 'bold' }}>Score: {" "}</Text>
              {game["metadata"].teamScore} -{" "}
              {game["metadata"].opposingScore}
            </Text>
          </View>
          <View style={styles.dataRow}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  if (global.vibrate) Haptics.impactAsync('light')
                  navigation.navigate("GameStatistics", { gameSave: game })
                }}
              >
                <Text style={styles.buttonText}>Game Details</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    );
  }

  calculateAvgScore() {
    let labels= [],
        data= [];
    // slice() to create a copy and avoid reversing the original array
    this.state.gameArray.slice().reverse().forEach(game => {
      let totalScore = 0;
      let count = 0;
      labels.push(game["metadata"].ID);
      game["stats"].forEach(shot => {
        if (shot.chosenScore != null){
          totalScore += shot.chosenScore;
          count ++;
        }
      });
      data.push(count > 0 ? totalScore/count : 0);
    });
    return {"labels": labels, "data": data};
  }

  renderLifetimeStatsChart() {
    const statsObject = this.calculateAvgScore();
    const data = {
      labels: statsObject.labels,
      datasets: [{data: statsObject.data }]
    };
    return (
      <View style={styles.chartContainer}>
        <Text style={styles.chartTitleText}>Average Score by Shot</Text>
        <LineChart
          data={data}
          width={wp('80%')} // from react-native
          height={wp('60%')}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#b43d2e',
            backgroundGradientTo: '#b43d2e',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}
          bezier
          fromZero={true}
          style={styles.chart}
        />
      </View>
    )
  }

  renderLifetimeStatsBox() {
    return (
      <View style={styles.box} >
        <View style={styles.boxHeader}>
            <Text style={styles.headerText}> Lifetime Stats</Text>
            <TouchableOpacity
                style={styles.button1}
                onPress={() => {
                  if (global.vibrate) Haptics.impactAsync('light')
                  this.setState({ showLifetimeStats: !this.state.showLifetimeStats })
                }}
              >
              <Text style={styles.buttonText2}> {this.state.showLifetimeStats ? "-" : "+"} </Text>
            </TouchableOpacity>

        </View>
        {this.state.showLifetimeStats? this.renderLifetimeStatsChart(): null}
      </View>
    );
  }

  render() {
    return this.state.gamesLoaded ? (
      <ScrollView
        contentContainerStyle={styles.scrollStyle}
        scrollIndicatorInsets={{right: 1}}
      >
        {this.state.gameArray.length? this.renderLifetimeStatsBox() : null}
        {this.state.gameArray.map(this.renderGames.bind(this))}
      </ScrollView>
    ) : (
      <View style={styles.scrollStyle}>
        <View style={styles.spinnerContainer}>
          <ActivityIndicator size='large' color='#b43d2e' />
          <Text style={{ marginTop: wp('1%') }}>Loading...</Text>
        </View>
      </View>
    );
  }
};

export default PriorGames;
