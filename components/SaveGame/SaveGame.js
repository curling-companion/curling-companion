import React, { Component }  from 'react'
import styles from "./Styles";
import { ScrollView, View, Text, TouchableOpacity, Keyboard } from 'react-native';
import createSecureStore from "@neverdull-agency/expo-unlimited-secure-store";
import {
  TextField,
} from 'react-native-material-textfield';
import * as Haptics from 'expo-haptics';

const secureStore = createSecureStore();

const OptionsEnum = {
  OUTCOMEOFGAME: 'outcomeofgame',
};
Object.freeze(OptionsEnum);


const outcomeofgameOpts = [
  { label: 'Win', value: 0 },
  { label: 'Lose', value: 1 }
];

class SaveGame extends Component {
  constructor() {
    super();
    this.state = {
      ID: null,
      chosenOutcome: 0,
      gameName: "",
      teamScore: "",
      opposingScore: "",
      saved: false,
      nameError: '',
      teamError: '',
      oppError: '',
      date: null,
      loaded: false
    }
  }

  async componentDidMount() {
    // setup date
    const day = new Date().getDate();
    // + 1 since getMonth returns 0-11 instead of 1-12
    const month = new Date().getMonth() + 1;
    const year = new Date().getFullYear();
    const date = `${month}-${day}-${year}`

    // setup gameID
    // 1. set the gameID based on how many games we have
    // on local storage
    const getCurrentGameIDKey = 'CurrentGameID'
    let gameID = await secureStore.getItem(getCurrentGameIDKey)
    gameID = gameID ? parseInt(gameID) + 1 : 1

    this.setState({
      ID: gameID,
      date: date,
      loaded: true
    })
  }

  gameNameRef = React.createRef();
  teamScoreRef = React.createRef();
  oppScoreRef = React.createRef();

  validateInt = (num) => /^\d+$/.test(num);

  onChangeGameName = (text) => {
    if (text.length === 0) {
      this.setState({ gameName: '', nameError: '' });
    } else {
      this.setState({ gameName: text, nameError: ''})
    }
  }

  onChangeTeamScore = (text) => {
    if (text.length === 0) {
      this.setState({ teamScore: '', teamError: '' });
    } else if (!this.validateInt(text)) {
      this.setState({ teamError: 'Score must be a number' });
    } else if (Number(text) > 99) {
      this.setState({ teamError: 'Score cannot exceed 99' });
    } else {
      this.setState({ teamScore: text, teamError: '' });
    }
  }

  onChangeOppScore = (text) => {
    if (text.length === 0) {
      this.setState({ opposingScore: '', oppError: '' });
    } else if (!this.validateInt(text)) {
      this.setState({ oppError: 'Score must be a number' });
    } else if (Number(text) > 99) {
      this.setState({oppError: 'Score cannot exceed 99'});
    } else {
      this.setState({ opposingScore: text, oppError: '' });
    }
  }

  // Use input to determine game winner
  determineGameOutcome = (teamScore, oppScore) => {
    if (teamScore >= oppScore) {
      this.setState({ chosenOutcome: 0 });
    } else {
      this.setState({ chosenOutcome: 1 });
    }
  }

  isValidSubmit = () => {
    let isValid = true;
    const { teamScore, opposingScore, gameName } = this.state;
    const { current: nameField} = this.gameNameRef, { current: teamField } = this.teamScoreRef, { current: oppField } = this.oppScoreRef;

    if (nameField.isErrored()) {
      isValid = false;
    } else if (gameName.length === 0) {
      this.setState({ nameError: 'Field must not be empty'});
      isValid = false;
    }

    if (teamField.isErrored()) {
      isValid = false;
    } else if (teamScore.length === 0) {
      this.setState({ teamError: 'Field must not be empty'});
      isValid = false;
    }

    if (oppField.isErrored()) {
      isValid = false;
    } else if (opposingScore.length === 0) {
      this.setState({oppError: 'Field must not be empty'});
      isValid = false;
    }

    if (!isValid && global.vibrate) Haptics.impactAsync('heavy')

    return isValid;
  }

  createGameSave = () => {

    // 2. construct the game save object
    // gameStats houses all the shots data passed in from
    // a previous screen
    let gameStats = this.props.route.params.game
    let gameInfo = {
      ID: this.state.ID,
      date: this.state.date,
      chosenOutcome: this.state.chosenOutcome,
      gameName: this.state.gameName,
      teamScore: this.state.teamScore,
      opposingScore: this.state.opposingScore
    }
    let gameSave = {
      metadata: gameInfo,
      stats: gameStats
    }

    return gameSave
  }

  handleSave = async () => {
    if (this.isValidSubmit()) {
      try {
        const { navigation } = this.props;
        let { teamScore, opposingScore } = this.state;
        teamScore = parseInt(teamScore);
        opposingScore = parseInt(opposingScore);
        await this.determineGameOutcome(teamScore, opposingScore);
        const gameSave = this.createGameSave()
        this.saveGame(gameSave)
        this.setState({ saved: true })
        alert('Game saved')
        // Need to fix later, hacky solution
        navigation.navigate("GameStatistics", {
          gameSave: gameSave,
          gameID: this.state.ID
        })
      } catch (error) {
        console.log(error);
        alert('Game failed to save');
      }
    }
  }

  saveGame = (gameSave) => {

    const getCurrentGameIDKey = 'CurrentGameID'

    // 3. save the gameSave object to local storage
    const storeGameKey = 'Game' + this.state.ID.toString()
    secureStore.setItem(storeGameKey, JSON.stringify(gameSave))
      .catch((err) => {throw (err)})
  }

  render() {
    return this.state.loaded ? (
      <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps='never'>
        <View style={styles.cardContainer}>
          <View style={styles.card}>
            <View style={styles.cardTitle}>
              <Text style={styles.cardTitleText}>Post-Game</Text>
            </View>
              <TextField
                label='Game name'
                onChangeText={this.onChangeGameName}
                containerStyle={styles.textFieldContainer}
                value={this.state.gameName}
                error={this.state.nameError}
                ref={this.gameNameRef}
              />
              <TextField
                label="Your team's score"
                keyboardType='number-pad'
                onChangeText={this.onChangeTeamScore}
                containerStyle={styles.numFieldContainer}
                value={this.state.teamScore}
                error={this.state.teamError}
                ref={this.teamScoreRef}
              />
              <TextField
                label="Opposing team's score"
                keyboardType='number-pad'
                onChangeText={this.onChangeOppScore}
                containerStyle={styles.numFieldContainer}
                value={this.state.opposingScore}
                error={this.state.oppError}
                ref={this.oppScoreRef}
              />
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                if (global.vibrate) Haptics.impactAsync('light')
                this.handleSave()
              }}
            >
              <Text style={styles.buttonText}>Save & Review</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    ) : (
      <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps='never'>
        <View style={styles.cardContainer}></View>
        </ScrollView>
    );
  }
};

export default SaveGame;
