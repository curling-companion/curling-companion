### the process of saving to local storage

  1. retrieve the last game ID from local storage
    a) if we get a valid response back, then incremeant that
    game ID by one to save a new game (i.e if we have 10 games,
    the next one will hold game ID 11)
    b) if a valid response was not recieved (i.e null), then
    this means that we have no games saved in local storage
    as such, make the game ID of the game that we wanna save = 1

  2. after setting the correct game ID, construct the
  game save object, it holds
    a) game metadata
      game ID, outcome, game name, scores
    b) game stats

  3. save the game to local storage

  * keys that are used
    * CurrentGameID -> gets the last game ID
    * Game + ID # -> gets the gameSave object for that game
      but needs to be reverted back to regular JSON

  * everything that uses secureStore is asynchronous,
  so either use async/await or .then to handle promises
