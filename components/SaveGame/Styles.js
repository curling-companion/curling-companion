import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    backgroundColor: '#E5FFFF',
  },
  cardContainer: {
    backgroundColor: '#e5ffff',
    borderRadius: 10,
    width: wp('100%'),
    marginTop: hp('1.5%'),
    alignItems: 'center'
  },
  card: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    paddingBottom: wp('3%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: .9
  },
  cardTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#aa1b1c',
    borderRadius: 50,
    height: hp('5%'),
    marginTop: hp('2%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    shadowColor: '#adadc3',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1
  },
  cardTitleText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp('4.3%'),
  },
  buttonContainer: {
    justifyContent: "center"
  },
  button: {
    height: hp("7%"),
    width: wp("35%"),
    borderRadius: wp("10%"),
    marginTop: wp('3%'),
    backgroundColor: "#7e0006",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  buttonText: {
    color: "white",
    fontSize: hp("2%")
  },
  textFieldContainer: {
    width: '50%',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  numFieldContainer: {
    width: '50%',
    justifyContent: 'center',
    alignSelf: 'center'
  }
});

export default styles;
