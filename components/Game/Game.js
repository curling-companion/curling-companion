import React, { Component } from "react";
import styles, { stepIndicatorStyles } from "./Styles";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import {
  listenOrientationChange,
  removeOrientationListener
} from "react-native-responsive-screen";
import Shot from "../Shot/Shot";
import StepIndicator from "react-native-step-indicator";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as Haptics from 'expo-haptics';


class Game extends Component {
  constructor() {
    super();
    this.state = {
      numbOfEnds: 0,
      currentShot: 0,
      shotStats: []
    };
  }

  componentDidMount() {
    listenOrientationChange(this);
    const { numbOfEnds } = this.props.route.params;
    this.setState({ numbOfEnds }, () => this.initializeShotStats());
  }

  componentWillUnMount() {
    removeOrientationListener();
  }

  initializeShotStats = () => {
    let shotStats = [], length = this.state.numbOfEnds * 8;
    for (let i = 0; i < length; i++) {
      shotStats.push(
        {
          chosenRotation: 0,
          chosenShotType: 0,
          chosenScore: 1,
          chosenWeightMissed: null,
          chosenLineMissed: null,
          sweepError: null,
          lineError: null,
          otherError: ""
        }
      )
    }
    this.setState({ shotStats });
  };

  displayCurrentPlayer = () => {
    const { currentShot } = this.state;
    let currShotDisplay = 1;
    if (currentShot % 2 === 1) {
      currShotDisplay = 2;
    }
    if (currentShot % 8 < 2) {
      return <Text style={styles.shotText}>Lead Shot #{currShotDisplay}</Text>;
    } else if (currentShot % 8 < 4) {
      return (
        <Text style={styles.shotText}>Second Shot #{currShotDisplay}</Text>
      );
    } else if (currentShot % 8 < 6) {
      return <Text style={styles.shotText}>Third Shot #{currShotDisplay}</Text>;
    } else {
      return <Text style={styles.shotText}>Skip Shot #{currShotDisplay}</Text>;
    }
  };

  handleRadioPress = (optionName, value) => {
    let shotStats = [...this.state.shotStats];
    let shotStat = {...shotStats[this.state.currentShot]};
    if (shotStat[optionName] === value
      && optionName !== "chosenScore"
      && optionName !== "chosenRotation"
      && optionName !== "chosenShotType"
      //everything but score, rotation, and type are nullable.
    ) {
      shotStat[optionName] = null;
    } else {
      shotStat[optionName] = value;
    }
    shotStats[this.state.currentShot] = shotStat;
    this.setState({ shotStats });
  };

  handleTextInput = (value) => {
    let shotStats = [...this.state.shotStats];
    let shotStat = {...shotStats[this.state.currentShot]};
    shotStat.otherError = value;
    shotStats[this.state.currentShot] = shotStat;
    this.setState({ shotStats });
  };

  handleOnPrevious = () => {
    if (this.state.currentShot !== 0) {
      let newCurrShot = this.state.currentShot;
      newCurrShot--;
      this.setState({
        currentShot: newCurrShot
      });
    }
  };

  handleOnNext = () => {
    let thisShot = this.state.shotStats[this.state.currentShot];
    // console.log(thisShot);
    if (thisShot.chosenScore === 0 
      && thisShot.chosenWeightMissed === null
      && thisShot.chosenLineMissed === null
      && thisShot.sweepError === null
      && thisShot.lineError === null
      // we will need to add a check that the "other" reason is also
      // empty, once that feature is merged in.
      ) {
      return alert('Please select a reason this shot was missed.');
    }
    let newCurrShot = this.state.currentShot;
    newCurrShot++;
    this.setState({
      currentShot: newCurrShot
    });
  };

  handleOnSubmit = () => {
    const { navigation } = this.props;
    navigation.navigate("SaveGame", {
      game: this.state.shotStats,
    });
  };

  render() {
    const { numbOfEnds, currentShot } = this.state;
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          contentContainerStyle={styles.shotContainer}
          keyboardOpeningTime={0}
          enableOnAndroid
        >
          <Shot
            shotStats={this.state.shotStats.length === 0 ? { } : this.state.shotStats[currentShot]}
            handleRadioPress={this.handleRadioPress}
            handleTextInput={this.handleTextInput}
            otherError={this.state.shotStats.length === 0 ? { } : this.state.shotStats[currentShot].otherError}
          />
        </KeyboardAwareScrollView>
        <View>
          <View style={styles.headerContainer}>
            <View style={styles.headerTextContainer}>
              <Text style={styles.headerText}>End </Text>
              <Text style={styles.headerText}>{this.displayCurrentPlayer()}</Text>
            </View>
            <StepIndicator
              customStyles={stepIndicatorStyles}
              currentPosition={currentShot / 8}
              stepCount={numbOfEnds}
            />
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                if (global.vibrate) Haptics.impactAsync('light')
                this.handleOnPrevious()
              }}
            >
              <Text style={styles.buttonText}>Previous</Text>
            </TouchableOpacity>
            {currentShot === numbOfEnds * 8 - 1 ? (
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  if (global.vibrate) Haptics.impactAsync('light')
                  this.handleOnSubmit()
                }}
              >
                <Text style={styles.buttonText}>Finish</Text>
              </TouchableOpacity>
            ) : (
                <TouchableOpacity style={styles.button} onPress={() => {
                  if (global.vibrate) Haptics.impactAsync('light')
                  this.handleOnNext()
                }}
                >
                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    );
  }
}

export default Game;
