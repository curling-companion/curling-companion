import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    paddingBottom: hp('.5%'),
    paddingTop: hp('.5%'),
    backgroundColor: '#4d0000',
    width: '100%',
  },
  headerTextContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: hp('2%'),
    paddingRight: hp('2%')
  },
  headerText: {
    height: hp('3%'),
    fontSize: hp('2%'),
    marginBottom: hp('.3%'),
    marginTop: hp('.3%'),
    color: 'white'
  },
  shotContainer: {
    flexGrow: 1,
    paddingBottom: 150,
    alignItems: 'center',
    backgroundColor: '#e5ffff'
  },
  buttonContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    backgroundColor: '#4d0000',
    paddingTop: hp('.4%'),
    paddingBottom: hp('5%')
  },
  button: {
    height: hp("5%"),
    width: wp("25%"),
    borderRadius: wp("10%"),
    margin: 5,
    backgroundColor: "#7e0006",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: '#2c0000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  buttonText: {
    color: "white",
    fontSize: hp("2%")
  }
});

export const stepIndicatorStyles = {
  stepIndicatorSize: hp('3%'),
  currentStepIndicatorSize: hp('4.3%'),
  separatorStrokeWidth: hp('.5%'),
  currentStepStrokeWidth: hp('.5%'),
  stepStrokeCurrentColor: '#b43d2e',
  separatorFinishedColor: '#b43d2e',
  separatorUnFinishedColor: '#979799',
  stepIndicatorFinishedColor: '#b43d2e',
  stepIndicatorUnFinishedColor: '#979799',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: hp('1.9%'),
  currentStepIndicatorLabelFontSize: hp('1.9%'),
  stepIndicatorLabelCurrentColor: '#000000',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
  labelColor: '#ffffff',
  labelSize: 15,
  currentStepLabelColor: '#7E0006'
};

export default styles;
