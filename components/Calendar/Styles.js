import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    width: wp('95%'),
    backgroundColor: '#f5F5F6',
  },

  eventContainer: {
    textAlign: 'left', 
    height: hp('42%'),
    marginTop : hp('5%'),
  },

  eventText: {
    color: 'black',
    fontSize: wp('4%'),
    textAlign: 'center',
  },

  eventDate: {
    color: 'black',
    fontSize: wp('4%'),
    lineHeight: wp('7%'),
  },

  eventLocation: {
    color: 'black',
    fontSize: wp('4%'),
    lineHeight: wp('7%'),
  },

  eventTitle: {
    color: 'black',
    fontSize: wp('4%'),
    lineHeight: wp('7%'),
  },

  button: {
    height: hp("7%"),
    width: wp("100%"),
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
    borderTopWidth: 0.5,
    borderColor: 'lightgrey',
  },

  buttonContainer: {
    position: 'absolute',
    bottom: 0,
  },
  
  buttonText: {
    color: "black",
    fontWeight: '500',
    fontSize: hp("2%")
    
  },
  arrow: {
    color: 'black', 
    fontSize: hp("2.5%"),
    fontWeight: '500',
  }
});

export default styles;
