export const events = [
  {dateStart:'2021-01-21', dateEnd:'2021-01-31', title: '2021 Winter World University Games', location: 'Lucerne, Switzerland'},

  {dateStart:'2021-02-20', dateEnd: '2021-02-27', title: '2021 World Junior Championships', location: 'Beijing, China'},

  {dateStart:'2021-02-06', dateEnd:'2021-02-13', title: '2021 National Championships', location: 'Cedar Rapids, Iowa, USA'},

  {dataStart: '2021-11-13', dateEnd: '2021-11-21', title: '2022 US Olympic Team Trials', location: 'Omaha, Nebraska'},

  {dateStart: '2021-03-20', dateEnd: '2021-03-28', title: '2021 Women\'s World Championship', location: 'Schaffhausen, Switzerland'},

  {dateStart: '2022-02-04', dateEnd: '2022-02-20', title: '2022 Winter Olymic Games', location: 'Beijing, China'},

  {dateStart: '2022-03-04', dateEnd: '2022-03-13', title: '2022 Paralympic Winter Games', location: 'Beijing, China'} ,

  {dateStart: '2026-02-06', dateEnd: '2026-02-22', title: '2026 Olympic Winter Games', location: 'Cortina, Italy'},

  {dateStart: '2026-03-06', dateEnd: '2026-03-15', title: '2026 Paralympic Winter Games', location: 'Cortina, Italy'} , 
];

export const calendarTheme = { 
  calendarBackground: '#E5FFFF',
  textSectionTitleColor: 'black',
  todayTextColor: 'red',
  arrowColor: 'black', 
  textMonthFontWeight: 'bold',
  textDayHeaderFontWeight: '500',
  textMonthFontSize: 20,
  textDayHeaderFontSize: 15,
  textDayFontWeight: '400',
};