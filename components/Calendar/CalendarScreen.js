import React, { Component }  from 'react';
import { View, Text, TouchableOpacity, Button, MaskedViewComponent } from "react-native";
import {Calendar, CalendarList} from 'react-native-calendars';
import moment from "moment";
import styles from "./Styles";
import {events, calendarTheme} from "./eventList"; 

class CalendarScreen extends Component {
  constructor() {
    super();
    this.state = {
      eventList : false,
      selectedDate : moment(new Date()).format("YYYY-MM-DD"),
    };
    this.markedDates = this.buildMarkedDatesObject();
    this.currentDate = moment(new Date()).format("YYYY-MM-DD");
  }

  nextDate(i){
    return moment(i, "YYYY-MM-DD").add(1,'days').format( "YYYY-MM-DD");
  }

  buildMarkedDatesObject() {
    let obj = {}; 
    events.forEach(item => {
      obj[item.dateStart] = {
          selected: true, 
          startingDay: true, 
          color: '#bdf2e9',
          textColor: 'black', 
          title: item.title, 
          location: item.location};
      for( let i = this.nextDate(item.dateStart); i< item.dateEnd; 
           i = this.nextDate(i)) {
        obj[i] = {
            selected: true, 
            color: '#bdf2e9', 
            textColor: 'black', 
            title: item.title, 
            location: item.location}
      }
      obj[item.dateEnd] = {
          selected: true, 
          endingDay: true, 
          color: '#bdf2e9', 
          textColor: 'black', 
          title: item.title, 
          location: item.location}; 
    });
    return obj;
  }

  updateMarkedDatesObject() {
    let options = {...this.markedDates[this.state.selectedDate], 
                ...{selected : true, 
                    color: '#81ebd9', 
                    textColor: this.state.selectedDate == this.currentDate ? 'red' : 'blue'}
                  };
    let obj = {...this.markedDates, ...{[this.state.selectedDate]: options}};
    return obj;
  }

  displayEventList() {
    if(this.markedDates[this.state.selectedDate]) {
      return (
        <View style= {styles.sectionContainer}>
          <View style={styles.eventContainer}>
            <Text style={styles.eventTitle}>{'\t'}Event:{'\t'}
            <Text style= {{fontWeight: 'bold'}}>{this.markedDates[this.state.selectedDate].title}</Text></Text>
            <Text style={styles.eventDate}>{'\t'}Date:{'\t'}{moment(this.state.selectedDate).format('DD-MM-YYYY')}</Text>
            <Text style={styles.eventLocation}>{'\t'}Location:{'\t'}{this.markedDates[this.state.selectedDate].location}</Text>
          </View>   
        </View>
      )
    };
    return (
      <View style= {styles.sectionContainer}>
        <View style={styles.eventContainer}>
          <Text style={styles.eventTitle}><Text style= {{textAlign: 'center',}}>{'\n'} No Events</Text></Text>
        </View>
      </View>
      
    );
  }  

  displayButton(){
    return (
      <View style= {styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => this.setState({eventList : false})}>
          <Text style={styles.buttonText} >Back</Text>
        </TouchableOpacity>
      </View>
    );
  }

  pastScrollRange() {
    let monthsDiff = moment(this.state.selectedDate).month() - moment().month(),
        yearsDiff = moment(this.state.selectedDate).year()-moment().year();
    return monthsDiff + yearsDiff * 12; 
    
  }
  
  render() {
    let component; 
    if(this.state.eventList) {
      component = <Calendar 
                    style={{height: 380}}
                    current={this.state.selectedDate}
                    minDate={this.currentDate}
                    onDayPress={(day) => this.setState({selectedDate: day.dateString})} 
                    onPressArrowRight={addMonth => {
                      this.setState({selectedDate: moment(this.state.selectedDate, 'YYYY-MM-DD').add(1, 'M').format('YYYY-MM-DD')}),
                      addMonth()
                    }}
                    onPressArrowLeft={substractMonth => {
                      let day = moment(this.state.selectedDate, 'YYYY-MM-DD').subtract(1, 'M').format('YYYY-MM-DD');
                      day > this.currentDate ? this.setState({selectedDate: day}) : this.setState({selectedDate: this.currentDate}),
                      substractMonth()
                    }}
                    renderArrow={(direction) => {
                      if(direction === 'left') {
                        let selected = moment(this.state.selectedDate, "YYYY-MM-DD"),
                            current = moment(this.currentDate, "YYYY-MM-DD");
                        if(selected.month() == current.month() && selected.year() == current.year()) return null;
                        return <Text style= {styles.arrow}>{'<'}</Text>
                      }
                      return <Text style= {styles.arrow}>{'>'}</Text>
                    }}
                    markedDates={this.updateMarkedDatesObject()}
                    markingType={'period'}
                    theme = {calendarTheme}
                  />;
    } else {
      component = <CalendarList 
                    onDayPress={(day) => this.setState({eventList : true, selectedDate: day.dateString})}
                    current={this.state.selectedDate}
                    minDate={this.currentDate}
                    pastScrollRange={this.pastScrollRange()}
                    futureScrollRange={12}
                    markedDates={this.markedDates}
                    markingType={'period'}
                    theme = {calendarTheme}
                  />;
    } 
   
    return (
      <View>
        {component}
        {this.state.eventList ?  this.displayEventList() : null}
        {this.state.eventList ?  this.displayButton() : null}
      </View>
    );
  }
}
export default CalendarScreen;
