import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';
import { Overlay } from 'react-native-elements';
import { MaterialIcons } from 'react-native-vector-icons';
import createSecureStore from "@neverdull-agency/expo-unlimited-secure-store"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import * as Haptics from 'expo-haptics';

const secureStore = createSecureStore()

export default class Settings extends Component {
    constructor(props){
        super(props);
        this.state = {
            isVisible: false
        };
    }

    openingO = () => {
        this.setState({isVisible:!this.state.isVisible})
    }

    handleClick = (option) => {
      global[option] = !global[option]
      if (option == 'vibrate' && global.vibrate) Haptics.impactAsync('heavy')
      this.forceUpdate()
    }

    clearGameHistory = async() => {
      await secureStore.removeItem("CurrentGameID")
      this.openingO()
    }
    render(){
        return (
          <View style={styles.setting}>
            <MaterialIcons
              style={styles.settings}
              name="settings"
              size={35}
              onPress={this.openingO}
            />
            {this.state.isVisible ? (
              <Overlay
                isVisible={this.state.isVisible}
                width="auto"
                height="auto"
                onBackdropPress={this.openingO}
              >
                <View>
                  <TouchableOpacity
                    onPress={() => this.handleClick("vibrate")}
                    style={styles.buttonContainer}
                  >
                    <Text
                      style={[
                        styles.button,
                        { color: global.vibrate ? "green" : "red" },
                      ]}
                    >
                      Vibrate
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.handleClick("sound")}
                    style={styles.buttonContainer}
                  >
                    <Text
                      style={[
                        styles.button,
                        { color: global.sound ? "green" : "red" },
                      ]}
                    >
                      Sound
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={this.clearGameHistory}
                    style={styles.buttonContainer}
                  >
                    <Text style={[styles.button, { color: "#007AFE" }]}>
                      Clear game history
                    </Text>
                  </TouchableOpacity>
                </View>
              </Overlay>
            ) : null}
          </View>
        );
    }
}

  const styles = StyleSheet.create({
    settings:{

        top: 25,
        left: 150,
        position: 'absolute',
    },

    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonContainer: {
      marginBottom: 12
    },
    button: {
      fontSize: hp('2.6%'),
      textAlign: "center"
    }
  });
