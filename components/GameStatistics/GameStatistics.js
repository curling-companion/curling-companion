import React, { Component } from "react";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import styles from "./Styles";
import {
  listenOrientationChange,
  removeOrientationListener,
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
  } from "react-native-responsive-screen";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from 'react-native-chart-kit';
import createSecureStore from "@neverdull-agency/expo-unlimited-secure-store";
import * as Haptics from 'expo-haptics';

const secureStore = createSecureStore();


class GameStatistics extends Component {
  constructor() {
    super();
    this.state = {
      expanded: [false, false, false, false],
    }
  };

  componentDidMount() {
    listenOrientationChange(this);
  };

  componentWillUnMount() {
    removeOrientationListener();
  };

  // Positions
  // 0 - Team
  // 1 - Lead
  // 2 - Second
  // 3 - Third
  // 4 - Skip

  // Calculate score and number of guards based on position
  calculateGuards = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    // Calculate all position guard counts
    let guardScores = [0, 0, 0, 0, 0];
    let guardCount = [0, 0, 0, 0, 0];
    stats.forEach((shot, i) => {
      if (shot.chosenShotType === 0) {
        // This check may be unnecessary if score cannot be null
        if (shot.chosenScore != null){
          // Always update team
          guardScores[0] += shot.chosenScore;
          guardCount[0]++;
          // Update the specific player
          guardScores[Math.floor((i%8)/2)+1] += shot.chosenScore;
          guardCount[Math.floor((i%8)/2)+1]++;
        }
      }
    });
    // Return the specified position
    // return guardScores[pos]/guardCount[pos];
    return {score: guardScores[pos], count: guardCount[pos]}
  };

  // Calculate score and number of draws based on position
  calculateDraws = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    // Calculate all position guard counts
    let drawScores = [0, 0, 0, 0, 0];
    let drawCount = [0, 0, 0, 0, 0];
    stats.forEach((shot, i) => {
      if (shot.chosenShotType === 1) {
        // This check may be unnecessary if score cannot be null
        if (shot.chosenScore != null){
          // Always update team
          drawScores[0] += shot.chosenScore;
          drawCount[0]++;
          // Update the specific player
          drawScores[Math.floor((i%8)/2)+1] += shot.chosenScore;
          drawCount[Math.floor((i%8)/2)+1]++;
        }
      }
    });
    // Return the specified position
    // return drawScores[pos]/drawCount[pos];
    return {score: drawScores[pos], count: drawCount[pos]}
  };

  // MAY WANT TO CHANGE THIS TO TAKEOUTS INSTEAD OF HITS
  // Calculate score and number of guards based on position
  calculateHits = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    // Calculate all position guard counts
    let hitScores = [0, 0, 0, 0, 0];
    let hitCount = [0, 0, 0, 0, 0];
    stats.forEach((shot, i) => {
      if (shot.chosenShotType === 2) {
        // This check may be unnecessary if score cannot be null
        if (shot.chosenScore != null){
          // Always update team
          hitScores[0] += shot.chosenScore;
          hitCount[0]++;
          // Update the specific player
          hitScores[Math.floor((i%8)/2)+1] += shot.chosenScore;
          hitCount[Math.floor((i%8)/2)+1]++;
        }
      }
    });
    // Return the specified position
    return {score: hitScores[pos], count: hitCount[pos]}
  };

  // Calculates the average score of all shots taken for a
  // given position
  calculateAverage = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    let score = 0;
    let count = 0;
    stats.forEach((shot, i) => {
      if(pos==0){
        score += shot.chosenScore;
        count++;
      } else{
        if(pos == Math.floor((i%8)/2)+1){
          score += shot.chosenScore;
          count++;
        }
      }
    });
    return Math.round(score/count*25);
  };

  // Iterates through the data and grabs all the shots for
  // the given position and returns it as an array
  getAllShots = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    let shots = [];
    stats.forEach((shot, i) => {
      if(pos==0){
        shots.push(shot.chosenScore);
      } else{
        if(pos == Math.floor((i%8)/2)+1){
          shots.push(shot.chosenScore);
        }
      }
    });
    return shots;
  };

  // Gets labels for the shot progression for players
  getLabels = (len) => {
    let labels = [];
    for(let i=0; i<len; i++){
      if(i%2==0){
        labels.push('E'+String(parseInt(i/2)+1));
      } else{
        labels.push('');
      }
    }
    return labels;
  };

  // Gets labels for shot progression for the team
  getTeamLabels = (len) => {
    let labels = [];
    for(let i=0; i<len; i++){
      if(i%8==0){
        labels.push('E'+String(parseInt(i/8)+1));
      } else{
        labels.push('');
      }
    }
    return labels;
  }

  // Renders a text line with the distribution of each
  // shot type as a percentage of total shots taken by
  // that given position
  renderShotDistribution = (pos) => {
    const { gameSave } = this.props.route.params;
    let length = gameSave["stats"].length;
    // Need to reduce the total count when not calculating for the team
    if (pos!=0) {
      length /= 4;
    }
    let guard = this.calculateGuards(pos);
    let draw = this.calculateDraws(pos);
    let hit = this.calculateHits(pos);

    let guardPerc = Math.round(guard['count']/length*100);
    let drawPerc = Math.round(draw['count']/length*100);
    let hitPerc = Math.round(hit['count']/length*100);

    const pieData = [
      {
        name: 'Guards',
        population: guardPerc,
        color: '#b43d2e',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
      {
        name: 'Draws',
        population: drawPerc,
        color: '#de8b78',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
      {
        name: 'Hits',
        population: hitPerc,
        color: '#ffd7cc',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
    ];

    return (
      <PieChart
        data={pieData}
        width={wp('80%')}
        height={wp('60%')}
        chartConfig={{
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
        }}
        bezier
        accessor="population"
        backgroundColor="transparent"
        paddingLeft={wp('5%')}
      />
    );

  };

  // Renders a BarChart with the average score per shot
  // for the given position
  renderShotAverages = (pos) => {
    let guard = this.calculateGuards(pos);
    let draw = this.calculateDraws(pos);
    let hit = this.calculateHits(pos);

    let guardScore = guard['count'] > 0 ? guard['score']/guard['count'] : 0;
    let drawScore = draw['count'] > 0 ? draw['score']/draw['count'] : 0;
    let hitScore = hit['count'] > 0 ? hit['score']/hit['count'] : 0;

    const BarData = {
      labels: ['Guard', 'Draw', 'Hit'],
      datasets: [
        {
          // data: [3.5, 3, 2],
          data: [
            guardScore, drawScore, hitScore,
          ],
          strokeWidth: 2, // optional
        },
      ],
    };

    return (
      <BarChart
        data={BarData}
        width={wp('80%')} // from react-native
        height={wp('60%')}
        chartConfig={{
          backgroundColor: '#b43d2e',
          backgroundGradientFrom: '#b43d2e',
          backgroundGradientTo: '#b43d2e',
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {

            borderRadius: 16
          }
        }}
        fromZero={true}
        bezier
        style={styles.chart}
      />
    );
  };

  // Renders all shot scores for the given position
  // in a line graph
  renderProgressionChart = (pos) => {
    let shots = this.getAllShots(pos);
    let labels = pos==0 ? this.getTeamLabels(shots.length) : this.getLabels(shots.length);
    const line = {
      labels: labels,
      datasets: [
        {
          data: shots,
          strokeWidth: 2, // optional
        },
      ],
    };

    return (
        <LineChart
          data={line}
          width={wp('80%')} // from react-native
          height={wp('60%')}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#b43d2e',
            backgroundGradientTo: '#b43d2e',
            decimalPlaces: 0, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}
          bezier
          fromZero={true}
          style={styles.chart}
        />
    );
  };

  // Renders a bar chart with the distribution of
  // missed shots by rotation selection
  renderMissedRotation = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    let inturn = 0;
    let outturn = 0;
    stats.forEach((shot, i) => {
      if(pos==0){
        if(shot.chosenScore == 0){
          if(shot.chosenRotation == 0){inturn++;}
          else{outturn++;}
        }
      } else{
        if(pos == Math.floor((i%8)/2)+1){
          if(shot.chosenScore == 0){
            if(shot.chosenRotation == 0){inturn++;}
            else{outturn++;}
          }
        }
      }
    });

    const pieData = [
      {
        name: 'In-turn',
        population: inturn,
        color: 'rgba(131, 167, 234, 1)',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
      {
        name: 'Out-turn',
        population: outturn,
        color: '#F00',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
    ];

    return (
      <PieChart
        data={pieData}
        width={wp('80%')}
        height={wp('60%')}
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#fb8c00',
          backgroundGradientTo: '#ffa726',
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16
          }
        }}
        bezier
        style={styles.chart}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="15"
        absolute
      />
    );
  }

  // Renders a bar chart with the distribution of
  // missed shots by weight
  renderMissedWeight = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    let light = 0;
    let heavy = 0;
    stats.forEach((shot, i) => {
      if(pos==0){
        if(shot.chosenScore == 0){
          if(shot.chosenWeightMissed == 0){light++;}
          else{heavy++;}
        }
      } else{
        if(pos == Math.floor((i%8)/2)+1){
          if(shot.chosenScore == 0){
            if(shot.chosenWeightMissed == 0){light++;}
            else{heavy++;}
          }
        }
      }
    });

    const pieData = [
      {
        name: 'Light',
        population: light,
        color: 'rgba(131, 167, 234, 1)',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
      {
        name: 'Heavy',
        population: heavy,
        color: '#F00',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
    ];

    return (
      <PieChart
        data={pieData}
        width={wp('80%')}
        height={wp('60%')}
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#fb8c00',
          backgroundGradientTo: '#ffa726',
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16
          }
        }}
        bezier
        style={styles.chart}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="15"
        absolute
      />
    );
  }

  // Renders a bar chart with the distribution of
  // missed shots by line
  renderMissedLine = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    let inside = 0;
    let wide = 0;
    stats.forEach((shot, i) => {
      if(pos==0){
        if(shot.chosenScore == 0){
          if(shot.chosenLineMissed == 0){inside++;}
          else{wide++;}
        }
      } else{
        if(pos == Math.floor((i%8)/2)+1){
          if(shot.chosenScore == 0){
            if(shot.chosenLineMissed == 0){inside++;}
            else{wide++;}
          }
        }
      }
    });

    const pieData = [
      {
        name: 'Inside',
        population: inside,
        color: 'rgba(131, 167, 234, 1)',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
      {
        name: 'Wide',
        population: wide,
        color: '#F00',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
    ];

    return (
      <PieChart
        data={pieData}
        width={wp('80%')}
        height={wp('60%')}
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#b43d2e',
          backgroundGradientTo: '#7e0006',
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16
          }
        }}
        bezier
        style={styles.chart}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="15"
        absolute
      />
    );
  }

  // Renders a bar chart with the distribution of
  // missed shots by line
  renderMissedError = (pos) => {
    const { gameSave } = this.props.route.params;
    let stats = gameSave["stats"];
    let sweep = 0;
    let line = 0;
    stats.forEach((shot, i) => {
      if(pos==0){
        if(shot.chosenScore == 0){
          if(shot.sweepError != null){sweep++;}
          if(shot.lineError != null){line++;}
        }
      } else{
        if(pos == Math.floor((i%8)/2)+1){
          if(shot.chosenScore == 0){
            if(shot.sweepError != null){sweep++;}
            if(shot.lineError != null){line++;}
          }
        }
      }
    });

    const pieData = [
      {
        name: 'Sweep',
        population: sweep,
        color: 'rgba(131, 167, 234, 1)',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
      {
        name: 'Line',
        population: line,
        color: '#F00',
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      },
    ];

    return (
      <PieChart
        data={pieData}
        width={wp('80%')}
        height={wp('60%')}
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#fb8c00',
          backgroundGradientTo: '#ffa726',
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16
          }
        }}
        bezier
        style={styles.chart}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="15"
        absolute
      />
    );
  }

  toggleBoxExpand = (boxID) => {
      let temp = this.state.expanded.slice();
      temp[boxID] = !temp[boxID];
      this.setState({expanded: temp});
  };

  renderExpandableBoxes = () => {
      let boxArray = [], names = ["Team", "Lead", "Second", "Third", "Skip"];
      const { navigation } = this.props;
      const { gameSave } = this.props.route.params;

      for (let i = 0; i < 5; i++) {
          boxArray.push(
              <View style={styles.box} key={i}>
                <View style={styles.boxHeader}>
                  <Text style={styles.headerText}> {names[i]} - {this.calculateAverage(i)}%</Text>
                  <TouchableOpacity
                      style={styles.button}
                      onPress={() => {
                        if (global.vibrate) Haptics.impactAsync('light')
                        this.toggleBoxExpand(i)
                      }}
                    >
                      <Text style={styles.buttonText}> {this.state.expanded[i] ? "-" : "+"} </Text>
                    </TouchableOpacity>
                </View>
                {
                  this.state.expanded[i]
                    ?
                    <View>
                      <View style={styles.chartContainer}>
                        <Text style={styles.chartTitleText}>Shot Distribution</Text>
                          {this.renderShotDistribution(i)}
                        <Text style={styles.chartTitleText}>Average Score by Shot</Text>
                          {this.renderShotAverages(i)}
                        <Text style={styles.chartTitleText}>Shot Progression</Text>
                        {this.renderProgressionChart(i)}
                        { i != 0 ?
                        <TouchableOpacity
                          style={styles.advancedButton}
                          onPress={() => {
                            if (global.vibrate) Haptics.impactAsync('light')
                            navigation.navigate("AdvancedStats", { gameStats: gameSave["stats"], player: i })
                          }}
                          >
                          <Text style={styles.buttonText}>  Advanced Stats </Text>
                        </TouchableOpacity> : null
                        }
                      </View>
                    </View>
                    : null
                }
              </View>
          );
      };
      return boxArray;
  };

  handleOnHome = async () => {
    const { navigation } = this.props;
    const { gameID } = this.props.route.params;
    const CurrentGameIDKey = 'CurrentGameID'
    try {
      await secureStore.setItem(CurrentGameIDKey, gameID.toString())
    } catch (error) {
      // app will still navigate to Home despite the error
      alert('Game failed to save');
      console.log(error)
    }
    navigation.navigate("Home")
  }

  render() {
    const { navigation } = this.props;
    const { gameSave } = this.props.route.params;

    return (
        <View style={styles.container}>
            <ScrollView contentContainerStyle={styles.scrollContainer}>
              <View style={styles.header}>
                <Text style={styles.gameName}>{gameSave['metadata'].gameName}</Text>
              </View>
              <View style={styles.scoreboardContainer}>
                <View style={styles.left}>
                  <Text style={styles.teamText}>Team 1</Text>
                </View>
                <View style={styles.middle}>
                  <Text style={styles.scoreText}>{gameSave['metadata'].teamScore} - {gameSave['metadata'].opposingScore}</Text>
                </View>
                <View style={styles.right}>
                  <Text style={styles.teamText}>Team 2</Text>
                </View>
              </View>
                {this.renderExpandableBoxes()}
                <TouchableOpacity
                  style={styles.homeButton}
                  onPress={() => {
                    if (global.vibrate) Haptics.impactAsync('light')
                    this.handleOnHome()
                  }}
                >
                  <Text style={styles.homeButtonText}>Home</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
  };
};

export default GameStatistics;
