import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5FFFF',
  },
  headerContainer: {
    paddingBottom: hp('.5%')
  },
  header: {
    fontSize: hp('3%'),
    marginBottom: hp('1.5%'),
    textAlign: 'center',
  },
  box: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    marginTop: hp('1.5%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: .9,
    paddingTop: hp('.7%'),
    paddingBottom: hp('.7%')
  },
  headerText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('6%'),
  },
  scrollContainer: {
    flexGrow: 1,
    paddingBottom: 50,
    alignItems: 'center',
  },
  boxHeader: {
    flexDirection: 'row',
    width: wp('90%'),
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: hp('.6%')
  },
  button: {
    height: hp("3%"),
    width: hp("3%"),
    borderRadius: hp("6%"),
    backgroundColor: "#7E0006",
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: .5
  },
  advancedButton: {
    height: hp("4.5%"),
    width: wp("35%"),
    borderRadius: hp("6%"),
    marginBottom: wp('2%'),
    backgroundColor: "#7E0006",
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: .5
  },
  buttonText: {
    color: "white",
    fontWeight: 'bold',
  },
  homeButton: {
    height: hp("7%"),
    width: wp("35%"),
    borderRadius: wp("10%"),
    marginTop: wp('2.5%'),
    backgroundColor: "#7E0006",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: .5
  },
  homeButtonText: {
    color: "white",
    fontSize: hp("2%")
  },
  chartContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  chart: {
    marginVertical: 8,
    borderRadius: 16,
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  gameName: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('8%'),
    lineHeight: wp('8%') * 1.25
  },
  teamContainer: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  teamText: {
    fontSize: wp('5%'),
    lineHeight: wp('5%') * 1.25
  },
  scoreContainer: {
    width: '43%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  scoreText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('7.5%'),
    lineHeight: wp('7.5%') * 1.25
  },
  shotTypeContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#aa1b1c',
    borderRadius: 50,
    height: hp('5%'),
    marginTop: hp('2%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    marginBottom: hp('2.5%'),
    paddingLeft: wp('2%'),
    paddingRight: wp('2%')
  },
  shotTypeText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp('4%'),
  },
  chartTitleText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('4.3%'),
    marginTop: wp('4%')
  },
  scoreboardContainer: {
    flexDirection: 'row',
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '33.333%',
    paddingLeft: wp('7%')
  },
  middle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '33.333%',
  },
  right: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '33.333%',
    paddingRight: wp('7%')
  }
});

export default styles;
