import React, { Component }  from 'react'
import styles from "./Styles";
import { ScrollView, View, Text , TouchableOpacity, Image} from 'react-native';
import Unorderedlist from 'react-native-unordered-list';


class RulesScreen extends Component {

  constructor() {
    super();
    this.state = {
      showContent1 : false,
      showContent2 : false,
      showContent3 : false,
      showContent4 : false,
      expanded: [false, false, false, false],
    };
    this.icons = {
      up : require('./minus.png'),
      down : require('./plus.png'),
    };
    this.content = ["Curling has similarities to bowls and shuffleboard (deck) and involves sliding granite stones, also called rocks, into a target area at the other end of a long, thin, strip of ice which constitutes the \“pitch\.\"\n\nThe team that propels their rocks closest to the centre of the target scores points accordingly, with the path of the stone influenced by team members who sweep and brush the ice ahead of the stone in order to alter its speed or curl.",
                    "Curling is played between two teams of four using eight granite stones each.\n\nThe “pitch” is a flat, smooth area of ice measuring 45-46m long and 4.4-5m wide.\n\nThere is a “house” at each end, a circular target made up of a blue outer circle with a 12ft diameter, a white circle inside that with a diameter of eight feet and a red circle with a diameter of four feet.\n\nThe stones themselves weight between 17 and 20kg, are at least 11cm high with a maximum circumference of 91cm and have a handle attached to the top. They are made of granite and the handles are usually red for one side and yellow for the other.\n\nThe two sweepers who follow the rock down the ice use brushes or brooms, usually made of fiberglass and fabric or horsehair but there are no real restrictions on the materials from which it is constructed. In addition the players wear curling shoes which are broadly similar to standard trainers except that one sole is smooth to enable sliding.\n\nPlayers also usually use gloves, specific curling trousers and a stopwatch to better understand the pace of the ice and the need for sweeping.",
                    "Scoring is done after each “end” (an end being a set where both teams have thrown all eight stones) with whichever team is closest to the centre of the house being awarded a point.\n\nFurther points are awarded for each stone of theirs that is closer than the best of the opposition’s.\n\nIn order to score any points at least one stone must be \“in the house\", which is to say touching any of the circles or overhanging them (due to the shape of the stones).",
                  ]
    }

  toggleBoxExpand = (boxID) => {
    let temp = this.state.expanded.slice();
    temp[boxID] = !temp[boxID];
    this.setState({expanded: temp});
};

  displayContent(content) {
    return (
      <View style={styles.subcard}>
        <Text style={styles.rulesText}>
          {content}
         </Text>
      </View>
    );
  }

  displayRules() {
    return (
      <View style={styles.subcard}>
        <Unorderedlist bulletUnicode={0x29BF} style={styles.ul}>
          <Text style={styles.rulesText}>
            Teams of four take it in turns to curl two rocks towards the target area with the scores being counted after all 16 rocks have been sent down the ice.{"\n"}
          </Text>
        </Unorderedlist>
        <Unorderedlist bulletUnicode={0x29BF} style={styles.ul}>
          <Text style={styles.rulesText}>
            International matches have a time limit of 73 minutes per side with two timeouts lasting a minute each. 10 minutes and one timeout are permitted per extra end in the event of a tie.{"\n"}
          </Text>
        </Unorderedlist>
        <Unorderedlist bulletUnicode={0x29BF} style={styles.ul}>
          <Text style={styles.rulesText}>
            The stone must be released its front edge crosses a line called the hog. Foul throws are removed from the ice before they have come to rest or in contact with other rocks.{"\n"}
          </Text>
        </Unorderedlist>
        <Unorderedlist bulletUnicode={0x29BF} style={styles.ul}>
          <Text style={styles.rulesText}>
            Sweeping may be done by two members of the team up to the tee line, whilst after that point only one player can brush. After the tee one player from the opposing side may also sweep.{"\n"}
          </Text>
        </Unorderedlist>
        <Unorderedlist bulletUnicode={0x29BF} style={styles.ul}>
          <Text style={styles.rulesText}>
            A stone touched or moved when in play by a player or their broom will either be replaced or removed depending on the situation.{"\n"}
          </Text>
        </Unorderedlist>
        <Unorderedlist bulletUnicode={0x29BF} style={styles.ul}>
          <Text style={styles.rulesText}>
            The team to go first is decided by coin toss, “draw-to-the-button” contest or, in Olympic competition using win-loss records. Subsequently the team that failed to score in the previous end has the advantage of going last, called the hammer throw.{"\n"}
          </Text>
        </Unorderedlist>
        <Unorderedlist bulletUnicode={0x29BF} style={styles.ul}>
          <Text style={styles.rulesText}>
            A team may concede if they feel they cannot win, although depending on the event and stage of event they may have to wait until a certain number of ends have been completed.{"\n"}
          </Text>
        </Unorderedlist>
        <Unorderedlist bulletUnicode={0x29BF} style={styles.ul}>
          <Text style={styles.rulesText}>
            Fair play is of huge importance so there is a culture of self-refereeing with regards fouls and this is a big part of curling.{"\n"}
          </Text>
        </Unorderedlist>
      </View>
    );
  }
  renderExpandableBoxes = () => {
    let boxArray = [], names = [" Object of the Game", "Players & Equipment", "Scoring", "Rules of Curling"];
    const { navigation } = this.props;

    for (let i = 0; i < 4; i++) {
        boxArray.push(
            <View style={styles.box} key={i}>
              <View style={styles.boxHeader}>
                <Text style={styles.headerText}> {names[i]}</Text>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.toggleBoxExpand(i)}
                  >
                    <Text style={styles.buttonText}> {this.state.expanded[i] ? "-" : "+"} </Text>
                  </TouchableOpacity>
              </View>
              {
                this.state.expanded[i]
                  ?
                  <View>
                    <View style={styles.chartContainer}>
                      {i != 3? this.displayContent(this.content[i]): this.displayRules()}
                    </View>
                  </View>
                  : null
              }
            </View>
        );
    };
    return boxArray;
  };

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        {this.renderExpandableBoxes()}

        <View style={styles.sourceContainer}>
          <Text style={{ fontWeight: 'bold' }}>Source: </Text>
          <Text>https://www.rulesofsport.com/sports/curling.html </Text>
        </View>

      </ScrollView>
  );
  }
};

export default RulesScreen;

