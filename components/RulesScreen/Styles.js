import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    backgroundColor: '#E5FFFF',
  },
  card: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    paddingBottom: wp('3%'),
    marginBottom: hp('1.5%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: .9
  },
  subcard: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    paddingBottom: wp('.1%'),
    paddingRight: wp('5%'),
    paddingLeft: wp('5%')
  },
  subsubcard: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('70%'),
    paddingBottom: wp('3%'),
    paddingRight: wp('5%'),
    paddingLeft: wp('5%')
  },
  cardTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#b43d2e',
    borderRadius: 50,
    height: hp('5%'),
    marginTop: hp('2%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    shadowColor: '#adadc3',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1
  },
  cardContainer: {
    backgroundColor: '#e5ffff',
    borderRadius: 10,
    width: wp('100%'),
    marginTop: hp('1.5%'),
    alignItems: 'center'
  },
  sectionHeaderContainer: {
    backgroundColor: '#F5F5F6',
    width: wp('90%'),
    marginTop: hp('1.5%'),
    marginBottom: hp('1.5%'),
  },
  sectionContainer: {
    backgroundColor: '#f5F5F6',
    borderRadius: 10,
    width: wp('95%'),
    marginTop: hp('1.5%'),
    alignItems: 'center',
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 1,
    shadowOpacity: .5

  },
  headerContainer: {
    paddingBottom: hp('.5%')
  },
  header: {
    fontSize: hp('3%'),
    marginBottom: hp('1.5%'),
    textAlign: 'center',
  },
  headerText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('6%'),
  },
  box: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    marginTop: hp('1.5%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: .9,
    paddingTop: hp('.7%'),
    paddingBottom: hp('.7%')
  },
  boxHeader: {
    flexDirection: 'row',
    width: wp('90%'),
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: hp('.6%')
  },
  button: {
    height: hp("3%"),
    width: hp("3%"),
    borderRadius: hp("6%"),
    backgroundColor: "#7E0006",
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: .5
  },
  buttonText: {
    color: "white",
    fontWeight: 'bold',
  },
  rulesContainer: {
    backgroundColor: '#F5F5F6',
    width: wp('90%'),
    marginBottom: hp('1.5%'),
  },
  rulesText: {
    color: 'black',
    fontSize: wp('4%')
  },
  sourceText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('3%'),
  },
  optionText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('5%'),
  },
  ul: {
    fontSize: wp('3.8%'),
  },
  image: {
    width: wp('5%'),
    height: wp('5%'),
  },
  sourceContainer: {
    flexDirection: 'row',
    marginTop: wp('1%'),
    marginBottom: wp('6.5%'),
    fontSize: wp('3.8%'),
  }
});

export default styles;
