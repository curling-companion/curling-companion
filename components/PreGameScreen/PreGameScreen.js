import React, { Component } from "react";
import styles from "./Styles";
import { View, Text, TouchableOpacity } from "react-native";
import {
  listenOrientationChange,
  removeOrientationListener, widthPercentageToDP as wp
} from "react-native-responsive-screen";

import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button";
import * as Haptics from 'expo-haptics';

const endOpts = [
  { label: "6", value: 6 },
  { label: "8", value: 8 },
  { label: "10", value: 10 }
];

class PreGameScreen extends Component {
  constructor() {
    super();
    this.state = { chosenEnds: 6 };
  }

  componentDidMount() {
    listenOrientationChange(this);
  }

  componentWillUnMount() {
    removeOrientationListener();
  }

  // Hack-y but less refactoring!
  returnSelectedRadio = (i) => {
    switch (i) {
      case 0:
        return 6;
      case 1:
        return 8;
      case 2:
        return 10;
      default:
        console.log('Radio selection failed.');
        return 0;
    }
  }

  renderRadioButtons = () => {
    return (
      <RadioForm
        formHorizontal={true}
        animation={false}
      >
        {
          endOpts.map((obj, i) => (
            <RadioButton
              labelHorizontal={false}
              key={i}
            >
              <RadioButtonInput
                obj={obj}
                index={i}
                onPress={() => {
                  const { value } = obj;
                  console.log(value);
                  if (global.vibrate) Haptics.impactAsync('light')
                  this.setState({ chosenEnds: value });
                }}
                isSelected={this.state.chosenEnds === this.returnSelectedRadio(i)}
                borderWidth={wp('.5%')}
                buttonInnerColor={'#A0060F'}
                buttonOuterColor={'#A0060F'}
                buttonSize={wp('5%')}
                buttonOuterSize={wp('7.4%')}
                buttonStyle={{ marginRight: wp('2.2%') }}
                buttonWrapStyle={{}}
              />
              <RadioButtonLabel
                obj={obj}
                index={i}
                onPress={() => {
                  const { value } = obj;
                  this.setState({ chosenEnds: value });
                }}
                labelHorizontal={true}
                labelStyle={styles.labelText}
                labelWrapStyle={{}}
              />
            </RadioButton>
          ))
        }
      </RadioForm>
    )
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.header3}>
            Select number of ends
          </Text>
        </View>
        {this.renderRadioButtons()}
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              navigation.navigate("Game", {
                numbOfEnds: this.state.chosenEnds
              })
              if (global.vibrate) Haptics.impactAsync('light')
            }}
          >
            <Text style={styles.buttonText}>Start Game</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default PreGameScreen;
