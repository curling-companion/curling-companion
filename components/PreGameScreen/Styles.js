import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E5FFFF",
    alignItems: "center"
  },
  titleContainer: {
    marginTop: hp("25%"),
    marginBottom: hp("5%")
  },
  title: {
    fontSize: wp("8%")
  },
  header3: {
    fontSize: wp("5%")
  },
  buttonContainer: {
    marginTop: wp('10%'),
    justifyContent: "center"
  },
  button: {
    height: hp("7%"),
    width: wp("35%"),
    borderRadius: wp("10%"),
    margin: 5,
    backgroundColor: "#7e0006",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  buttonText: {
    color: "white",
    fontSize: hp("2%")
  },
  labelText: {
    fontSize: wp('3.5%'),
    lineHeight: hp('3.5%') * 1.25,
    color: 'black',
    marginRight: wp('4.4%')
  }
});

export default styles;
