import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5FFFF',
  },
  cardContainer: {
    backgroundColor: '#e5ffff',
    borderRadius: 10,
    width: wp('100%'),
    marginTop: hp('1.5%'),
    alignItems: 'center'
  },
  card: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    paddingBottom: wp('3%'),
    marginBottom: hp('1.5%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: .9
  },
  subcard: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    paddingBottom: wp('.1%'),
    paddingRight: wp('5%'),
    paddingLeft: wp('5%')
  },
  subsubcard: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('70%'),
    paddingBottom: wp('3%'),
    paddingRight: wp('5%'),
    paddingLeft: wp('5%')
  },
  cardTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#b43d2e',
    borderRadius: 50,
    height: hp('5%'),
    marginTop: hp('2%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    shadowColor: '#adadc3',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1
  },
  cardSubtitle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#aa1b1c',
    borderRadius: 50,
    height: hp('4.5%'),
    marginTop: hp('2%'),
    marginBottom: hp('1%'),
    marginLeft: wp('30%'),
    marginRight: wp('30%'),
    shadowColor: '#adadc3',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1
  },
  cardTitleText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp('4.5%'),
  },
  cardSubtitleText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp('3.5%'),
  },
  headerContainer: {
    paddingBottom: hp('.5%'),
  },
  header: {
    height: hp('3%'),
    fontSize: hp('3%'),
    marginBottom: hp('1%'),
    marginTop: hp('1.5%'),
    textAlign: 'center'
  },
  box: {
    backgroundColor: '#f5f5f6',
    borderRadius: 10,
    width: wp('90%'),
    marginTop: hp('1.5%'),
    shadowColor: '#97A297',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: .9,
    paddingTop: hp('.7%'),
    paddingBottom: hp('.7%')
  },
  headerText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('6%'),
  },
  scrollContainer: {
    flexGrow: 1,
    paddingBottom: 50,
    alignItems: 'center',
  },
  boxHeader: {
    flexDirection: 'row',
    width: wp('90%'),
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: hp('.6%')
  },
  button: {
    height: hp("3%"),
    width: hp("3%"),
    borderRadius: hp("6%"),
    backgroundColor: "#7E0006",
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: "white",
    fontWeight: 'bold',
  },
  homeButton: {
    height: hp("7%"),
    width: wp("35%"),
    borderRadius: wp("10%"),
    margin: 5,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center"
  },
  homeButtonText: {
    color: "white",
    fontSize: hp("2%")
  },
  chartContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  chart: {
    marginVertical: 8,
    borderRadius: 16
  },
  gameName: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('6.5%'),
    height: hp('10%'),
  },
  teamContainer: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  teamText: {
    fontSize: wp('4%'),
  },
  scoreContainer: {
    width: '43%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  scoreText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('6%'),
  },
  shotTypeContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#aa1b1c',
    borderRadius: 50,
    height: hp('5%'),
    marginTop: hp('2%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    marginBottom: hp('2.5%'),
    paddingLeft: wp('2%'),
    paddingRight: wp('2%')
  },
  shotTypeText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp('4%'),
  },
  chartTitleText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: wp('4.3%'),
  },
  reasonText: {
    color: 'black',
    fontSize: wp('4%'),
  },
});

export default styles;
