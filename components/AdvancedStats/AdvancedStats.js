import React, { Component } from "react";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import styles from "./Styles";

import {
  listenOrientationChange,
  removeOrientationListener,
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
  } from "react-native-responsive-screen";
import * as Haptics from 'expo-haptics';

class AdvancedStats extends Component {
  constructor() {
    super();
    this.state = {
      expanded: [false, false],
    }
  };

  componentDidMount() {
    listenOrientationChange(this);
  };

  componentWillUnMount() {
    removeOrientationListener();
  };

  renderMissedWeight = (pos, rotation, type) => {
    const { gameStats } = this.props.route.params;
    let light = 0;
    let heavy = 0;
    gameStats.forEach((shot, i) => {
      if(pos==0){
        if(shot.chosenScore == 0 && shot.chosenRotation == rotation && shot.chosenShotType == type){
          if(shot.chosenWeightMissed == 0){light++;}
          else if(shot.chosenWeightMissed == 1) {heavy++;}
      } else{
        if(pos == Math.floor((i%8)/2)+1){
            if(shot.chosenScore == 0 && shot.chosenRotation == rotation && shot.chosenShotType == type){
                if(shot.chosenWeightMissed == 0){light++;}
                else if(shot.chosenWeightMissed == 1) {heavy++;}
              }
            }
      }
    }});

    return (
        <View style={styles.subsubcard}>
        <Text style={styles.reasonText}>{light} light</Text>
        <Text style={styles.reasonText}>{heavy} heavy</Text>
        </View>
    );
  }

  // Renders a bar chart with the distribution of
  // missed shots by line
  renderMissedLine = (pos, rotation, type) => {
    const { gameStats } = this.props.route.params;
    let inside = 0;
    let wide = 0;
    gameStats.forEach((shot, i) => {
      if(pos==0){
        if(shot.chosenScore == 0 && shot.chosenRotation == rotation && shot.chosenShotType == type){
          if(shot.chosenLineMissed == 0){inside++;}
          else if(shot.chosenLineMissed == 1){wide++;}
        }
      } else{
        if(pos == Math.floor((i%8)/2)+1){
          if(shot.chosenScore == 0 && shot.chosenRotation == rotation && shot.chosenShotType == type){
            if(shot.chosenLineMissed == 0){inside++;}
            else if(shot.chosenLineMissed == 1){wide++;}
          }
        }
      }
    });


    return (
        <View style={styles.subsubcard}>
        <Text style={styles.reasonText}>{inside} inside</Text>
        <Text style={styles.reasonText}>{wide} wide</Text>
        </View>
    );
  }

  // Renders a bar chart with the distribution of
  // missed shots by line
  renderMissedError = (pos, rotation, type) => {
    const { gameStats } = this.props.route.params;
    let sweep = 0;
    let line = 0;
    gameStats.forEach((shot, i) => {
      if(pos==0){
        if(shot.chosenScore == 0 && shot.chosenRotation == rotation && shot.chosenShotType == type){
          if(shot.sweepError != null){sweep++;}
          if(shot.lineError != null){line++;}
        }
      } else{
        if(pos == Math.floor((i%8)/2)+1){
          if(shot.chosenScore == 0 && shot.chosenRotation == rotation && shot.chosenShotType == type){
            if(shot.sweepError != null){sweep++;}
            if(shot.lineError != null){line++;}
          }
        }
      }
    });

    return (
        <View style={styles.subsubcard}>
        <Text style={styles.reasonText}>{sweep} sweep</Text>
        <Text style={styles.reasonText}>{line} line</Text>
        </View>
    );
  }

  renderMissedOther = (pos, rotation, type) => {
    const { gameStats } = this.props.route.params;
    let missReasons = [];

    gameStats.forEach((shot, i) => {
      if(pos == Math.floor((i%8)/2)+1){
        if(shot.otherError !== undefined && shot.otherError != "" && shot.chosenRotation == rotation && shot.chosenShotType == type){
            missReasons.push(shot.otherError);
      }
    }});

    console.log(missReasons);

    return (
        <View style={styles.subsubcard}>
        <Text style={styles.reasonText}>{ missReasons.length != 0 ? missReasons.join("\n"): "None" }</Text>
        </View>
    );
  }

  toggleBoxExpand = (boxID) => {
    let temp = this.state.expanded.slice();
    temp[boxID] = !temp[boxID];
    this.setState({expanded: temp});
  };

  renderExpandableBoxes = () => {
    let boxArray = [], rotations = [0, 1], rotationNames = ["In-turn", "Out-turn"];
    const { gameStats } = this.props.route.params;
    const { player } = this.props.route.params;



      for (let i = 0; i < 2; i++) {
          boxArray.push(
              <View style={styles.box} key={i}>
                <View style={styles.boxHeader}>
                  <Text style={styles.headerText}> {rotationNames[i]} </Text>
                  <TouchableOpacity
                      style={styles.button}
                      onPress={() => {
                        if (global.vibrate) Haptics.impactAsync('light')
                        this.toggleBoxExpand(i)
                      }}
                    >
                      <Text style={styles.buttonText}> {this.state.expanded[i] ? "-" : "+"} </Text>
                    </TouchableOpacity>
                </View>
                {
                  this.state.expanded[i]
                    ?
                    <View>

                      <View style={styles.subcard}>
            <View style={styles.shotTypeContainer}>
              <Text style={styles.shotTypeText}>Guards</Text>
            </View>
            <Text style={styles.chartTitleText}>Missed Shot: Weight</Text>
            {gameStats ? this.renderMissedWeight(player, i, 0) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Line</Text>
            {gameStats ? this.renderMissedLine(player, i, 0) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Errors</Text>
            {gameStats ? this.renderMissedError(player, i, 0) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Other Reasons</Text>
            {gameStats ? this.renderMissedOther(player, i, 0) : null }
            </View>
            <View style={styles.subcard}>
            <View style={styles.shotTypeContainer}>
              <Text style={styles.shotTypeText}>Draws</Text>
            </View>
            <Text style={styles.chartTitleText}>Missed Shot: Weight</Text>
            {gameStats ? this.renderMissedWeight(player, i, 1) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Line</Text>
            {gameStats ? this.renderMissedLine(player, i, 1) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Errors</Text>
            {gameStats ? this.renderMissedError(player, i, 1) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Other Reasons</Text>
            {gameStats ? this.renderMissedOther(player, i, 1) : null }
            </View>

            <View style={styles.subcard}>
            <View style={styles.shotTypeContainer}>
              <Text style={styles.shotTypeText}>Hits</Text>
            </View>
            <Text style={styles.chartTitleText}>Missed Shot: Weight</Text>
            {gameStats ? this.renderMissedWeight(player, i, 2) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Line</Text>
            {gameStats ? this.renderMissedLine(player, i, 2) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Errors</Text>
            {gameStats ? this.renderMissedError(player, i, 2) : null }
            <Text style={styles.chartTitleText}>Missed Shot: Other Reasons</Text>
            {gameStats ? this.renderMissedOther(player, i, 2) : null }
            </View>
                    </View>
                    : null
                }
              </View>
          );
      };
      return boxArray;
  }

  render() {
    const { player } = this.props.route.params;
    let names = ["Team", "Lead", "Second", "Third", "Skip"];
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrollContainer}>
          <View style={styles.header}>
              <Text style={styles.gameName}>{names[player]}</Text>
          </View>
            {this.renderExpandableBoxes()}
        </ScrollView>
        </View>

    );
  };
};

export default AdvancedStats;
