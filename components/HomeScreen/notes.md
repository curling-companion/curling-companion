### what we have done:

* we have exported Figma frames as SVG files
* we translated the SVG frames to valid React Native JSX using https://react-svgr.com/playground/?native=true
* we then imported the translated JSX to a component called CircularFrame in component/SvgComponents, and that component is then imported into HomeScreen.js

### what we have planned next:

* transparency in inner circle for dark mode feature or possible background color change.
* What else goes in the home screen?
  - Title?
  - Event Countdown?

### what questions do we have:

* what fonts and colors should we use? it is important that make a decision early on to reduce the amount of work that we have to do in regards to design in future sprints
