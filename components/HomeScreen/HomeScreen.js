import React, { Component } from "react";
import styles from "./Styles";
import { View, TouchableOpacity, StatusBar } from "react-native";
import {
  listenOrientationChange,
  removeOrientationListener
} from "react-native-responsive-screen";
import Settings from "../Settings/Settings";
import CircularFrame from "../SvgComponents/CircularFrame";
import RulesScreen from "../RulesScreen/RulesScreen";
import PriorGames from "../PriorGames/PriorGames";
import * as Haptics from 'expo-haptics';

global.vibrate = global.sound = true

class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      disableButton: false, 
    }
  }
  componentDidMount() {
    listenOrientationChange(this);
  }

  componentWillUnMount() {
    removeOrientationListener();
  }

  setTimeoutButton() {
    this.setState({disableButton: true});
    setTimeout(function(){
      this.setState({disableButton: false});
    }.bind(this),1000);
  }

  render() {
    const barStyle = Platform.OS === "ios" ? "dark-content" : "light-content"
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar barStyle={barStyle} />
        <View style={{ flex: 0.2, marginRight: 30 }}>
          <Settings/>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity
            disabled={this.state.disableButton}
            style={[styles.clickableOption, {
            }]}
            onPress={() => {
              this.setTimeoutButton(),
              navigation.navigate("PriorGames")
              if (global.vibrate) Haptics.impactAsync('light')
            }}
          />
          <TouchableOpacity
            disabled={this.state.disableButton}
            style={[
              styles.clickableOption,
              { right: 0 }
            ]}
            onPress={() => {
            this.setTimeoutButton(),
              navigation.navigate("RulesScreen")
              if (global.vibrate) Haptics.impactAsync('light')
            }}
          />
          <TouchableOpacity
            disabled={this.state.disableButton}
            style={[
              styles.clickableOption,
              { top: "50%" }
            ]}
            onPress={() => {
              this.setTimeoutButton(),
              navigation.navigate("CalendarScreen")
              if (global.vibrate) Haptics.impactAsync('light')
            }}

          />
          <TouchableOpacity
            disabled={this.state.disableButton}
            style={[
              styles.clickableOption,
              {
                top: "50%",
                right: 0
              }
            ]}

            onPress={() => {
              this.setTimeoutButton(),
              navigation.navigate("PreGameScreen")
              if (global.vibrate) Haptics.impactAsync('light')
            }}

          />
          <CircularFrame style={styles.frame} />
        </View>
      </View>
    );
  }
}

export default HomeScreen;
