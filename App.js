import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from './components/HomeScreen/HomeScreen';
import SaveGame from './components/SaveGame/SaveGame';
import Shot from './components/Shot/Shot';
import Game from './components/Game/Game';
import backIcon from "./assets/backIcon.png";
import PreGameScreen from "./components/PreGameScreen/PreGameScreen";
import RulesScreen from './components/RulesScreen/RulesScreen';
import PriorGames from './components/PriorGames/PriorGames';
import GameStatistics from './components/GameStatistics/GameStatistics';
import CalendarScreen from './components/Calendar/CalendarScreen';
import AdvancedStats from './components/AdvancedStats/AdvancedStats';

const buttonOverRide = (title) => ({
    headerBackImage: () => <Image style={styles.backButton} source={backIcon} />,
    title: title
});

const Stack = createStackNavigator();

const App = () => {
  return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
          />
          <Stack.Screen
            name="PreGameScreen"
            component={PreGameScreen}
            options={buttonOverRide("Setup")}
          />
          <Stack.Screen
            name="GameStatistics"
            component={GameStatistics}
            options={buttonOverRide("Statistics")}
          />
          <Stack.Screen
            name="CalendarScreen"
            component={CalendarScreen}
            options={buttonOverRide("Calendar")}
          />
          <Stack.Screen
            name="Shot"
            component={Shot}
            options={buttonOverRide("")}
          />
          <Stack.Screen
            name="Game"
            component={Game}
            options={buttonOverRide("Game")}
          />
          <Stack.Screen
            name="SaveGame"
            component={SaveGame}
            options={buttonOverRide("Post-Game")}
          />
          <Stack.Screen
            name="RulesScreen"
            component={RulesScreen}
            options={buttonOverRide("Curling Rules")}
          />
        <Stack.Screen
          name="PriorGames"
          component={PriorGames}
          options={buttonOverRide("History")}
          />
          <Stack.Screen
          name="AdvancedStats"
          component={AdvancedStats}
          options={buttonOverRide("Advanced Statistics")}
          />
        </Stack.Navigator>
      </NavigationContainer>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e5ffff",
    alignItems: "center",
    justifyContent: "center"
  },
  backButton: {
    width: 30,
    height: 30,
    marginLeft: wp("1%"),
    marginRight: wp("1%")
  }
});

export default App;
